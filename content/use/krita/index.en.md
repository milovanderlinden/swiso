---
title: Krita
icon: icon.png
replaces:
    - adobe-cloud
---

**Krita** is a professional raster graphics painting and animation editor aimed mainly at artists - available for Windows, Mac and Linux.

{{< infobox >}}
- **Website:**
    - [krita.org](https://krita.org/en/)
    - [Download](https://krita.org/en/download)
{{< /infobox >}}
