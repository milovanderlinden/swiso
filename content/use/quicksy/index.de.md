---
title: Quicksy
icon: icon.png
replaces:
    - whatsapp
    - facebook-messenger
---

**Quicksy** ist eine besonders einsteigerfreundliche Variante von XMPP. Die Registrierung läuft hier - wie bei WhatsApp - über die Telefonnummer ab. Kontakte aus Deinem Adressbuch, die ebenfalls Quicksy nutzen, werden Dir automatisch vorgeschlagen. Du kannst aber auch beliebige andere XMPP-Nutzer kontaktieren.

{{< infobox >}}
- **Android app:**
    - [Quicksy](https://play.google.com/store/apps/details?id=im.quicksy.client) ([FDroid](https://f-droid.org/de/packages/im.quicksy.client/))
{{< /infobox >}}