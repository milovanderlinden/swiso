---
title: Facebook
subtitle: Social Networks
provider: facebook
order: 
    - mastodon
    - friendica
aliases:
    - /ethical-alternatives-to-facebook/
---